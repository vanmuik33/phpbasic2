<?php
if (!isset($_POST['signUp'])) {
    die('');
}
include('connect.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'vendor/autoload.php';

header('Content-Type: text/html; charset=UTF-8');
$userName = $_POST['userName'];
$passWord = ($_POST['psw']);
$passWordRepeat = ($_POST['psw-repeat']);
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$email = filter_var($email, FILTER_VALIDATE_EMAIL);
$gender = $_POST['gender'];
$language = implode(',', $_POST['language']);
$error = '';
$sql_syntax = '';
$result = '';
//Kiểm tra tên đăng nhập này đã có người dùng chưa
$sql = "SELECT * FROM `USERS` WHERE `USER_NAME` = '$userName'";

if (mysqli_num_rows(mysqli_query($connect, $sql)) > 0) {
    $error .= '<li>User Name existed</li>';
}

$regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
if (!preg_match($regex, $email)) {
    $error .= '<li>Email address not valid</li>';
}

if (mysqli_num_rows(mysqli_query($connect, "SELECT EMAIL FROM USERS WHERE EMAIL='$email'")) > 0) {
    $error .= '<li>Email existed</li>';
}

$regex = "/^(?=.+[A-Z]).{8}$/";

if (!preg_match($regex, $passWord)) {
    $error .= '<li>Password tu 6 den 32 ky tu, can co 1 chu cai viet hoa</li>';
}

if ($passWord != $passWordRepeat) {
    $error .= '<li>Repeat password khong dung</li>';
}
else $passWord = md5($_POST['psw']);

if ($error == '') {
    $sql_syntax = "INSERT INTO USERS (`USER_NAME`,`PASSWORD`,`FIRST_NAME`,`LAST_NAME`,`EMAIL`,`GENDER`,`LANGUAGE`)
            VALUE ('$userName','$passWord','$firstName','$lastName','$email','$gender','$language')";

    if ($result = mysqli_query($connect, $sql_syntax)) {

        $expFormat = mktime(
            date("H"),
            date("i"),
            date("s"),
            date("m"),
            date("d") + 1,
            date("Y")
        );

        $expDate = date("Y-m-d H:i:s", $expFormat);
        $key = md5(2418 * 2 + $email);
        $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
        $key = $key . $addKey;
        // Insert Temp Table
        mysqli_query(
            $connect,
            "INSERT INTO `TOKEN_TEMP` (`EMAIL`, `KEY`, `expDATE`) VALUES ('" . $email . "', '" . $key . "', '" . $expDate . "');"
        );
        $output = '<p>Dear user,</p>';
        $output .= '<p>Please click on the following link to active your password.</p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p><a href="/phpbasic2/activeUser.php?key=' . $key . '&email=' . $email . '&action=active" target="_blank">Activation.php?key=' . $key . '&email=' . $email . '&action=active</a></p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p>Please be sure to copy the entire link into your browser. The link will expire after 1 day for security reason.</p>';
        $output .= '<p>If you did not request this forgotten password email, no action is needed, your password will not be reset.</p>';
        $output .= '<p>Thanks,</p>';
        $output .= '<p>AllPHPTricks Team</p>';
        $body = $output;
        $subject = "Activation Account - AllPHPTricks.com";
        $email_to = $email;
        $fromserver = "noreply@yourwebsite.com";

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = 'vanmuik33@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = 'ghfjstqryylrltxg';


        //Set who the message is to be sent from
        $mail->setFrom('noreply@gmail.com', 'First Last');

        //Set an alternative reply-to address
        $mail->addReplyTo('vanmuk33@yahoo.com', 'Yahoo Mail');

        //Set who the message is to be sent to
        $mail->AddAddress($email_to);

        $mail->IsHTML(true);
        // $mail->Sender = $fromserver; // indicates ReturnPath header
        $mail->Subject = $subject;
        $mail->Body = $body;

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        }
    }
}
?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register Successfully</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>

<body>
    <?php include('navbar.php'); ?>
    <?php
    if ($result) : ?>
        <h1 class="text-success">Register Successed!</h1>
        <p class="decs">You have registered and the activation mail is sent to your email. Click the activation link to activate you account.</p>
        <a href="/phpbasic2/">Về trang chủ</a>
    <?php else : ?>
        <p>Register Failed</p>
        <ul>
            <?php echo $error ?>
        </ul>
        <a href="signUp.php">Try again!</a>
    <?php endif; ?>
</body>

</html>