<?php
$avatar_default = 'https://muinv.lahvui.xyz/phpbasic2/avatar/default-avatar.jpg';
if (isset($_SESSION['userID'])) {
    $getAvatar_SQL = "SELECT `AVATAR` FROM `USERS` WHERE `USER_ID` = $_SESSION[userID]";
    if ($getAvatarDB = mysqli_query($connect, $getAvatar_SQL)) {
        $avatar = mysqli_fetch_assoc($getAvatarDB);
        if ($avatar['AVATAR'] != '')
            $avatar = $avatar['AVATAR'];
        else $avatar = $avatar_default;
    }
}
?>
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == TRUE) : ?>
                <a class="navbar-brand" href="/phpbasic2/admin">HOME</a>
            <?php else : ?>
                <a class="navbar-brand" href="/phpbasic2/">HOME</a>
            <?php endif; ?>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">

                <?php if (!isset($_SESSION['userID'])) : ?>
                    <li><a href="signIn.php"><span class="glyphicon glyphicon-log-in"></span> Sign In</a></li>
                    <li><a href="signUp.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <?php else : ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><img src="<?php echo $avatar ?>" alt="" style="height:20px; border-radius:50%"> <b class="caret"></b></a>
                        <ul class="dropdown-menu">

                            <?php if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == TRUE) : ?>
                                <li><a href="../profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                                <li><a href="/phpbasic2/admin/listUser.php"><span class="glyphicon glyphicon-list-alt"></span> List user</a></li>
                                <li><a href="../signOut.php"><span class="glyphicon glyphicon-log-out"></span> Sign out</a></li>
                            <?php else : ?>
                                <li><a href="profile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
                                <li><a href="/phpbasic2/signOut.php"><span class="glyphicon glyphicon-log-out"></span> Sign out</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>