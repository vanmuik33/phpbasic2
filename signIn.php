<?php
if (session_id() === '')
  session_start();
if (isset($_SESSION['userID'])) {
  header('location: /phpbasic2');
}
if (isset($_POST['signIn'])) {
  include('connect.php');

  $userID = $_POST['userID'];
  $passWord = $_POST['psw'];
  $remember = ((isset($_POST['remember'])) ? 1 : "");

  if (!$userID || !$passWord) {
    echo "User Name or Password is invalid";
    exit;
  }
  $passWord = md5($passWord);

  $checkUser_SQL = "SELECT * FROM `USERS` WHERE ((`USER_NAME` = '$userID' OR `EMAIL` = '$userID') AND `IS_DELETE` != 1)";
  $checkUser = mysqli_query($connect, $checkUser_SQL);

  if (mysqli_num_rows($checkUser) == 0) {
    echo "This username does not exist";
    exit;
  }

  $userDB = mysqli_fetch_array($checkUser);

  if ($userDB['INACTIVE'] == 0) {
    echo "Please active your account.";
    exit;
  }

  if ($passWord != $userDB['PASSWORD']) {
    echo "Password id invalid.";
    exit;
  }
  if ($remember = 1) {
    $_SESSION['start'] = time(); // Taking now logged in time.
    // Ending a session in 1 minutes from the starting time.
    $_SESSION['expire'] = $_SESSION['start'] + (15 * 60);
  }
  if ($userDB['ADMIN'] == 1) {
    $_SESSION['userID'] = $userDB['USER_ID'];
    $_SESSION['isAdmin'] = TRUE;
    $_SESSION['userLastName'] = $userDB['LAST_NAME'];
    header("location:/phpbasic2/admin");
    // exit;
  } else {
    $_SESSION['userID'] = $userDB['USER_ID'];
    $_SESSION['userLastName'] = $userDB['LAST_NAME'];
    header("location:/phpbasic2");
    // exit; 
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sign in</title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <style>

  </style>
</head>

<body>
  <?php include('navbar.php') ?>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h1 class="text-primary">Sign In</h1>
        <form action="" method="POST">
          <div class="form-group">
            <label for="userID">User Name:</label>
            <input type="text" class="form-control" id="userID" name="userID">
          </div>
          <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="psw">
          </div>
          <div class="checkbox">
            <label><input type="checkbox" name="remember"> Remember me</label>
          </div>
          <div class="form-group">
            <a href="signUp.php">Sign Up</a> | <a href="forgotpw.php">Forgot password</a>
          </div>
          <button type="submit" class="btn btn-primary" name="signIn">Sign in</button>
        </form>
      </div>
    </div>
  </div>

</body>

</html>