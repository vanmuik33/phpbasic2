<?php
if (session_id() === '')
    session_start();
include_once('../connect.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AdminPage</title>
    <?php include('../bootstrap3.php'); ?>
</head>

<body>
    <?php include('../navbar.php'); ?>
    <div class="container">
        <?php
        if (!isset($_SESSION['userID'])) echo '<h1 class="text-primary">ADMINPAGE</h1>';
        else {
            $now = time();
            if ($now - $_SESSION['expire'] > 15 * 60) {
                session_unset();
                session_destroy();
                header('location:/phpbasic2/');
            } else {
                $userDB_sql = "SELECT `LAST_NAME` FROM `USERS` WHERE `USER_ID` = " . $_SESSION['userID'];
                $getUserDB = mysqli_query($connect, $userDB_sql);
                $userDB = mysqli_fetch_assoc($getUserDB);
                echo '<h2 class="text-success">Welcome ' . $userDB['LAST_NAME'] . '</h2>';
            }
        }
        if (isset($_SESSION['updated'])) {
            echo '<h2 class="text-success"> Update profile successfully </h2>';
            unset($_SESSION['updated']);
        }

        ?>

    </div>
</body>

</html>