<?php
include('connect.php');
$error = '';
if (
  isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"])
  && ($_GET["action"] == "active") && !isset($_POST["action"])
) {
  $key = $_GET["key"];
  $email = $_GET["email"];
  $curDate = date("Y-m-d H:i:s");
  $query = mysqli_query(
    $connect,
    "SELECT * FROM `TOKEN_TEMP` WHERE `KEY`='" . $key . "' and `EMAIL`='" . $email . "';"
  );
  $row = mysqli_num_rows($query);
  if ($row == "") {
    $error .= '<h2>Invalid Link</h2>
<p>The link is invalid/expired. Either you did not copy the correct link
from the email, or you have already used the key in which case it is 
deactivated.</p>
<p><a href="/phpbasic2/reactive.php">
Click here</a> to resend active email.</p>';
  } else {
    $row = mysqli_fetch_assoc($query);
    $expDate = $row['expDATE'];
    if ($expDate >= $curDate) {
?>
      <!DOCTYPE html>
      <html lang="en">

      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Active User</title>
        <?php include('bootstrap3.php'); ?>
      </head>

      <body>
        <?php include('navbar.php');
        mysqli_query($connect, "DELETE FROM `TOKEN_TEMP` WHERE `EMAIL`='" . $email . "';");
        mysqli_query($connect,"UPDATE `USERS` SET `INACTIVE` = 1 WHERE `EMAIL`='" . $email . "';");
        ?>
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <h2>Your account is activated. Now, you can sign in to website!</h2>
              <a href="/phpbasic2/signIn.php">Click here to to Sign in.</a>
            </div>
          </div>
        </div>

      </body>

      </html>

<?php
    } else {
      $error .= "<h2>Link Expired</h2>
      <p>The link is expired. You are trying to use the expired link which as valid only 24 hours (1 days after request).<br /><br /></p>";
    }
  }
  if ($error != "") {
    echo "<div class='error'>" . $error . "</div><br />";
  }
}
?>