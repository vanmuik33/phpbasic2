<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <style>
        * {
            box-sizing: border-box;
        }

        /* Overwrite default styles of hr */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Add a blue text color to links */
        a {
            color: dodgerblue;
        }
    </style>
</head>

<body>
    <?php include('navbar.php'); ?>
    <form action="register.php" class="form-group" method="POST">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-primary">Sign Up</h1>
                    <p>Please fill in this form to create an account.</p>
                    <hr />
                    <div class="form-group">
                        <label for="userName"><b>User Name</b></label>
                        <input type="text" placeholder="User Name" name="userName" id="usertName" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label for="firstName"><b>First Name</b></label>
                        <input type="text" placeholder="First Name" name="firstName" id="firstName" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label for="lastName"><b>Last Name</b></label>
                        <input type="text" placeholder="Last Name" name="lastName" id="lastName" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter Email" name="email" id="email" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="psw" id="psw" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="psw-repeat"><b>Repeat Password</b></label>
                        <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" class="form-control" required />
                    </div>


                    <div class="form-group">
                        <label for=""><b>Gender</b></label> <br>
                        <label class="radio-inline">
                            <input type="radio" name="gender" checked value="0">Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="1">Female
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="2">Other
                        </label>
                    </div>
                    <div class="form-group">
                        <label for=""><b>Language</b></label> <br>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="1">Tiếng Việt
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="2">English
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="3">français
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="4">русский
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="5">中文
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="100">Other
                        </label>
                    </div>

                    <hr />
                    <p class="signin text-left">Already have an account? <a href="signIn.php">Sign in</a>.</p>

                    <p>
                        By creating an account you agree to our
                        <a href="#">Terms & Privacy</a>.
                    </p>

                    <button type="submit" class="btn btn-primary" name="signUp">Sign up</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                </div>
            </div>
        </div>
    </form>
</body>

</html>