<?php
include('connect.php');
$error = '';
if (
  isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"])
  && ($_GET["action"] == "reset") && !isset($_POST["action"])
) {
  $key = $_GET["key"];
  $email = $_GET["email"];
  $curDate = date("Y-m-d H:i:s");
  $query = mysqli_query(
    $connect,
    "SELECT * FROM `TOKEN_TEMP` WHERE `KEY`='" . $key . "' and `EMAIL`='" . $email . "';"
  );
  $row = mysqli_num_rows($query);
  if ($row == "") {
    $error .= '<h2>Invalid Link</h2>
<p>The link is invalid/expired. Either you did not copy the correct link
from the email, or you have already used the key in which case it is 
deactivated.</p>
<p><a href="/phpbasic2/forgotpw.php">
Click here</a> to reset password.</p>';
  } else {
    $row = mysqli_fetch_assoc($query);
    $expDate = $row['expDATE'];
    if ($expDate >= $curDate) {
?>
      <!DOCTYPE html>
      <html lang="en">

      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <?php include('bootstrap3.php'); ?>
      </head>

      <body>
        <?php include('navbar.php'); ?>
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <form method="post" action="" name="update">
                <input type="hidden" name="action" value="update" />
                <br />
                <label><strong>Enter New Password:</strong></label><br />
                <input type="password" name="pass1" maxlength="15" required />
                <br /><br />
                <label><strong>Re-Enter New Password:</strong></label><br />
                <input type="password" name="pass2" maxlength="15" required />
                <br /><br />
                <input type="hidden" name="email" value="<?php echo $email; ?>" />
                <input class="btn btn-primary" type="submit" value="Reset Password" />
              </form>
            </div>
          </div>
        </div>

      </body>

      </html>

<?php
    } else {
      $error .= "<h2>Link Expired</h2>
      <p>The link is expired. You are trying to use the expired link which as valid only 24 hours (1 days after request).<br /><br /></p>";
    }
  }
  if ($error != "") {
    echo "<div class='error'>" . $error . "</div><br />";
  }
} // isset email key validate end


if (
  isset($_POST["email"]) && isset($_POST["action"]) &&
  ($_POST["action"] == "update")
) {
  $error = "";
  $pass1 = mysqli_real_escape_string($connect, $_POST["pass1"]);
  $pass2 = mysqli_real_escape_string($connect, $_POST["pass2"]);
  $email = $_POST["email"];
  $curDate = date("Y-m-d H:i:s");
  if ($pass1 != $pass2) {
    $error .= "<p>Password do not match, both password should be same.<br /><br /></p>";
  }
  if ($error != "") {
    echo "<div class='error'>" . $error . "</div><br />";
  } else {
    $pass1 = md5($pass1);
    mysqli_query(
      $connect,
      "UPDATE `USERS` SET `PASSWORD`='" . $pass1 . "'
WHERE `EMAIL`='" . $email . "';"
    );

    mysqli_query($connect, "DELETE FROM `TOKEN_TEMP` WHERE `EMAIL`='" . $email . "';");

    echo '<div class="error"><p>Congratulations! Your password has been updated successfully.</p>
<p><a href="/phpbasic2/signIn.php">
Click here</a> to Login.</p></div><br />';
  }
}
?>